# 01 Introduction

## - Python introduction

1. Python 是相當熱門的語言，因為其高度包裝、可讀性及簡潔的語法，工程師可以在短短幾行內就完成相當複雜的工作，故工程師常常使用 Python 來進行數據分析、腳本撰寫以及自動化測試。

## - Python package installing

1. Python installing ([macos](https://www.python.org/downloads/macos/))

   ```bash
   # please open your terminal and key in the bellow command
   python3 --version
   pip3 --version
   ```

   ![check_install](./image/check_install.png)

2. Installing some package (please download requirement.txt)

   ```bash
   sudo pip3 install -r requirement.txt
   ```

3. Jupyter tutorial (please open demo.ipynb)

   ```bash
   # please open your terminal and key in the bellow command
   jupyter notebook
   ```

   <img src="./image/jupyterDemo.png" alt="jupyterDemo" style="zoom:70%;" />

   <img src="./image/jupyterInterface.png" alt="jupyterInterface" style="zoom:100%;" />
