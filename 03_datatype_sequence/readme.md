# 03 Datatype - Sequence 

1. Sequence
   
   顧名思義，Sequence 類型的型別是有序的，可以在有序的資料結構裡存儲各種型別的資料。而 List 與 Tuple 之間唯一的差距就只有：List 可以任意更改元素內容及數量，Tuple 反之。
   
   - List
   
     ```python
     # The basic of List
     lst = ['Lance', 'Amy', 1, 2, 3]
     print(type(lst), len(lst), lst, sep = '\n')
     
     # Access List elements
     # 同學可以回想看看在前一天學到的 string type，也是一種近似於陣列的型別，而 list 的存取方式與 string 一模一樣
     print(lst[-1], lst[3:], sep = '\n')
     
     # Check elements
     # 這個例子裡有很多細節喔！請同學好好體會
     check_element = 'Lance'
     if 'Lance' in lst: # 檢查元素是否在 lst 裏面
     	print(f'{check_element} is in the lst.') # 格式化字串 -> format 使用
     else
     	print(f"{check_element} is't in the lst.") # single quote, double quote 聯合使用
     
     # -------- print --------
     # <class 'list'>
     # 5
     # ['Lance', 'Amy', 1, 2, 3]
     # 3
     # [2, 3]
     # Lance is in the lst.
     ```
   
     
   
   - Tuple
   
     
   
