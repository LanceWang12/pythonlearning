from header1.parent import parent

class child(parent):
    def __init__(self, a, b, c, d):
        super().__init__(a, b, c)
        self.d = d
    
    def demo_error(self):
        # child can't access private member from parent directly
        print(self.a, self._b, self.__c, self.d)
        
    def demo_right(self):
        print('Child accesses private member from parent in tricky:', self.a, self._b, self._parent__c, self.d)
        
    def __str__(self):
        print("CHild accesses private member by parent's method:", end = ' ')
        return super().__str__()