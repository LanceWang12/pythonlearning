# Module and Package


## Empty init file:
```
./demo.ipynb
./header1/  
├── __init__.py --> empty
├── child.py
├── parent.py  
└── __pycache__  
```
```python=
# in demo.ipynb
from header1.parent import parent
from header1.child import child

p = parent(1, 2, 3)
c = child(1, 2, 3, 4)
print(p)
print(c)
```
## Init file has some content:
```
./demo.ipynb
./header2/  
├── __init__.py --> has content
├── child.py
├── parent.py  
└── __pycache__  
```
```python=
# The content in __init__.py
from .child import child
from .parent import parent

__all__ = ["child", "parent"]
```
```python=
# in demo.ipynb
from header2 import parent
from header2 import child
# or
from header2 import *

p = parent(1, 2, 3)
c = child(1, 2, 3, 4)
print(p)
print(c)
```
###### tags: `python`