class parent():
    def __init__(self, a, b, c):
        self.a = a
        self._b = b
        self.__c = c
    def __str__(self):
        return f'a = {self.a}, b = {self._b}, c = {self.__c}'