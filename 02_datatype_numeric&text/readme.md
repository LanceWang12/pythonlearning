# 02 Datatype - Numeric and Text

1. Numeric
   
   除了最基本的加減乘除 (+, -, *, /)，同學可以參考 [連結](https://www.w3schools.com/python/python_operators.asp) 來學會更多的 operator(運算子)
   
   - int
   
     儲存**整數**型別的類別
   
     ```python
     a = 4
     print(type(a))
     # -------- print --------
     # <class 'int'>
     
     # example
     print((a + a + 2) / 2 * 100)
     # -------- print --------
     # 500.0
     ```
   
   - float
     儲存**浮點數**(小數)型別的類別
   
     ```python
     a = 4.1
     b = 4.1e3 # 科學記號表示法
     print(type(a), a)
     print(type(b), b)
     # -------- print --------
     # <class 'float'> 4.1
     # <class 'float'> 4100.0
     ```
     
   - complex
     儲存複數型別 (實數+虛數) 的類別
   
     ```python
     a = 11 + 5j
     b = -2j
     print(type(a), a)
     print(type(b), b)
     # -------- print --------
     # <class 'complex'> (11+5j)
     # <class 'complex'> (-0-2j)
     ```
     
     
     
   - bool (請搭配 day03 的 if else 敘述一起服用)
     儲存**布林**型別的類別
   
     ```python
     a = True # or False
     b = (10 > 2)
     c = (2 > 10)
     print(type(a), a)
     print(type(b), b)
     print(type(c), c)
     # -------- print --------
     # <class 'bool'> True
     # <class 'bool'> True
     # <class 'bool'> False
     
     # example
     if a:
       print("It is true.")
     else:
       print("It is false.")
     ```
   
2. Text
   Python 裏其實並沒有真正的 char 型別，對 Python 來說，char 是只有一個元素的string (字串)
   
   - string
     a. Ascii code 與字元間的轉換：電腦只能理解數字，故無法直接處理字元，所以宅宅們規定了一個 table 來儲存每個字元對應的編碼 (如 ASCII TABLE)，使電腦也可以處理字元。
   
     ```python
     # The conversion of ascii code and char
     a = ord('a')
     b = chr(a)
     print(f'a = {a}, b = {b}')
     print(f'type(a) = {type(a)}, type(b) = {type(b)}')
     
     # -------- print --------
     # a = 97, b = 'a'
     # type(a) = <class 'int'>, type(b) = <class 'str'>
     ```
   
     <img src="./image/asciiTable.png" alt="asciiTable" style="zoom:60%;" />
   
     b. 宣告方法：
   
     ```python
     # single quote, double quote, multi-quote
     
     # 不管是哪種括號，型別都屬於 string， Python 為了各種使用情境所以產生了不同的括號方式
     a = 'Hi'
     b = "Hi"
     c = 'Hi I am "Lance"' # single quote 可以囊括 double quote，反之亦然
     
     # 只有 multi-quote 可以跨行運作，同學可以試試看使用 「''」, 「""」跨行會如何？
     d = """
     Hi, I am Lance.
     I am teaching you how to program with Python.
     """
     print(a, b, c, d, sep = '\n')
     # -------- print --------
     # Hi
     # Hi
     # 
     # Hi, I am Lance.
     # I am teaching you how to program with Python.
     # 
     ```
     
     c. Array-like type
     
     ​	Python 的 string 是一種陣列 (array) 型態，可以走訪每個 element (元素)，也可以計算陣列的長度 (元素的個數)
     
     ```python
     # 取得陣列裡的某個元素
     a = "This is a string test."
     print(a[0], a[1], a[2], a[3])
     # -------- print --------
     # T h i s
     
     # Python 獨門祕技 - slicing
     print(a[2: 5]) # 由前往後取的第 2 ~ 5 的元素(不包含 5)
     print(a[-5: -2]) # 取的倒數 5 ~ 2 的元素(不包含 2)
     # -------- print --------
     # is 
     # tes
     
     # 各種省略方法
     print(a[: 5]) # 省略前面 -> 從 0 開始
     print(a[5: ]) # 省略後面 -> 最後一個元素結束
     print(a[:])   # 聰明的同學可以猜猜看省略前後是什麼意思？
     # -------- print --------
     # This 
     # is a string test.
     # This is a string test.
     ```
     
     d. format (格式化字串)：
     
     ​	處理字串時有一種情境：需要將班級每個同學的姓名及年齡都嵌入到同樣格式的字串裡面。Python 提供了 
     ​	format 函數供使用者使用
     
     ```python
     # error example - 若不使用 format，我們只能使用這種醜陋的方式來處理字串，而且還會出錯！！
     name = 'Lance'
     age = 10
     class_msg = "Hi, his(or her) name is " + name + ", and he(or she) is " + age + " year old."
     # -------- error message --------
     # Traceback (most recent call last):
     #   File "<stdin>", line 1, in <module>
     # TypeError: can only concatenate str (not "int") to str
     
     # using format
     # 最原始的使用方法
     class_msg1 = "Hi, his(or her) name is {}, and he(or she) is {} year old.".format(name, age)
     
     # 更換帶入順序
     class_msg2 = "Hi, his(or her) name is {1}, and he(or she) is {0} year old.".format(name, age)
     
     # 懶人寫法 - 在字串前面加上一個 'f'，就可以直接嵌入變數
     class_msg3 = f"Hi, his(or her) name is {name}, and he(or she) is {age} year old."
     
     print(class_msg1, class_msg2, class_msg3, sep = '\n')
     
     # -------- print --------
     # Hi, his(or her) name is Lance, and he(or she) is 10 year old.
     # Hi, his(or her) name is 10, and he(or she) is Lance year old.
     # Hi, his(or her) name is Lance, and he(or she) is 10 year old.
     ```
     
     
     
     b. [各種字串處理函數](https://www.w3schools.com/python/python_ref_string.asp)：
     
     ```python
     # 大小寫轉換(lower, upper, capitalize)
     a = 'HI I AM LANCE.'
     b = a.lower()
     c = a.capitalize()
     d = b.upper()
     print(a, b, c, d, sep = '\n')
     # -------- print --------
     # HI I AM LANCE.
     # hi i am lance.
     # Hi i am lance.
     # HI I AM LANCE.
     
     # count
     cnt = a.count('I')
     print(cnt)
     # -------- print --------
     # 2
     ```
