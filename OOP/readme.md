# Object Oriented Programming

## Introduction
1. Python 是完全物件導向的語言，所有變數皆為物件
2. POP(Procedual) vs OOP
    |                      |              OOP               |                        POP                         |
    |:--------------------:|:------------------------------:|:--------------------------------------------------:|
    |   Program Division   |        程式碼由物件組成        |              程式由函數(function)組成              |
    |     Data Control     | 每個物件皆可控管自己的成員變數 | 每個 function 即為獨立的個體，彼此之間無法共享參數 |
    |     Inheritence      |               無               |                         有                         |
    | Operator Overloading |               有               |                         無                         |
    |     Data hiding      |               有               |                         無                         |
    |  Using situation     |          處理複雜問題            |        不適合處理複雜問題                      | 
3. Class --->實體化---> Instance
```python=
class demo:
    def __init__(self):
        pass

# -------- 實體化 --------
a = demo()
```
## Inheritance (繼承)
這是 OOP 可以大幅簡化程式碼的原因之一，透過繼承 parent class，使 child 具有 parent 的特質
1. Single inheritance:
```python=
class parent():
    def __init__(self, a, b, c):
        self.a = a
        self._b = b
        self.__c = c
    def __str__(self):
        return f'a = {self.a}, b = {self._b}, c = {self.__c}'
    
class child(parent):
    def __init__(self, a, b, c, d):
        super().__init__(a, b, c)
        self.d = d
    
    def demo_error(self):
        # child can't access private member from parent directly
        print(self.a, self._b, self.__c, self.d)
        
    def demo_right(self):
        print('Child accesses private member from parent in tricky:', self.a, self._b, self._parent__c, self.d)
    
    def __str__(self):
        print("CHild accesses private member by parent's method:", end = ' ')
        return super().__str__()

# -------- demo --------
p = parent(1, 2, 3)
c = child(1, 2, 3, 4)
print(p, c, sep = '\n')

c.demo_right()
c.demo_error()
```
2. Multiple inheritance:
```python=
# Python example to show working of multiple 
# inheritance
class Base1(object):
    def __init__(self):
        self.str1 = "Geek1"
        print "Base1"
  
class Base2(object):
    def __init__(self):
        self.str2 = "Geek2"        
        print "Base2"
  
class Derived(Base1, Base2):
    def __init__(self):
          
        # Calling constructors of Base1
        # and Base2 classes
        Base1.__init__(self)
        Base2.__init__(self)
        print "Derived"
          
    def printStrs(self):
        print(self.str1, self.str2)
         
# -------- demo --------
ob = Derived()
ob.printStrs()
```
###### tags: `python`
